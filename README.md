# Бредогенератор

![License badge](https://raster.shields.io/badge/license-Do%20What%20The%20F*ck%20You%20Want%20To%20Public%20License%2C%20Version%202-lightgrey.png "License badge")

![Thumbnail](thumbnail.png?raw=true "Thumbnail")

_Бредогенератор_ — инструмент для генерирования сложных предложений, с насыщенной 
электротехнической терминологией, который подойдёт для заполнения различной 
технической документации, не требующей проверки содержания. Бредогенератор 
использует шаблоны предложений, в которые автоматически подставляются термины из
"встроенного" словаря.

## Демо

Увидеть проект в действии можно на [GitLab pages](https://ethicist.gitlab.io/generator/) (в разделе WEB-версия) или [здесь](https://gen.surge.sh/)

## Changelog

Последняя версия: 0.2 [Список изменений](CHANGELOG.md)

## License

This work is free. You can redistribute it and/or modify it under the
terms of the Do What The Fuck You Want To Public License, Version 2,
as published by Sam Hocevar. See the [LICENSE](LICENSE) file for more details.