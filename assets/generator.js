var num_toogle = 1;
var list_is_empty;
var a, b, c, d;
var al, bl, cl, dl;

function request_database() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            initialize(JSON.parse(this.responseText));
        }
    };
    xmlhttp.open("GET", "./database.json", true);
    xmlhttp.send();
}

function initialize(input) {
    a = new Array(input[0].length);
    b = new Array(input[1].length);
    c = new Array(input[2].length);
    d = new Array(input[3].length);
    al = a.length;
    bl = b.length;
    cl = c.length;
    dl = d.length;
    for (var i = 0; i < input[0].length; i++) {
        a[i] = input[0][i];
    }
    for (var i = 0; i < input[1].length; i++) {
        b[i] = input[1][i];
    }
    for (var i = 0; i < input[2].length; i++) {
        c[i] = input[2][i];
    }
    for (var i = 0; i < input[3].length; i++) {
        d[i] = input[3][i];
    }
    fill_text();
}

function rnd(limit) {
    return (Math.floor(Math.random() * limit));
}

function get_string(type) {
    var o;
    switch (type) {
    case 1:
        o = caps(a[rnd(al)]) + " " + b[rnd(bl)] + ", а также " +
        a[rnd(al)] + " " + b[rnd(bl)];
        break
    case 2:
        o = caps(a[rnd(al)]) + " " + b[rnd(bl)] + " и " + a[rnd(al)] +
        " " + b[rnd(bl)];
        break
    case 3:
        o = "Правила технической эксплуатации " + b[rnd(bl)] + " и " +
        a[rnd(al)] + " " + b[rnd(bl)] + " " + c[rnd(cl)];
        break
    case 4:
        o = caps(a[rnd(al)]) + " и " + a[rnd(al)] + ", а также " +
        a[rnd(al)] + " " + b[rnd(bl)] + " " + c[rnd(cl)];
        break
    case 5:
        o = d[rnd(dl)];
        break
    case 6:
        o = d[rnd(dl)] + ". " + d[rnd(dl)];
        break
    case 7:
        o = d[rnd(dl)] + ". " + caps(a[rnd(al)]) + " " + b[rnd(bl)] +
        ", а также " + a[rnd(al)] + " " + b[rnd(bl)];
        break
    case 8:
        o = d[rnd(dl)] + ". " + caps(a[rnd(al)]) + " " + b[rnd(bl)] +
        " и " + a[rnd(al)] + " " + b[rnd(bl)];
        break
    case 9:
        o = d[rnd(dl)] + ". " + d[rnd(dl)] + ". " + caps(a[rnd(al)]) +
        " и " + a[rnd(al)] + ", а также " + a[rnd(al)] + " " +
        b[rnd(bl)] + " " + c[rnd(cl)];
        break
    default:
        o = "Изучены " + a[rnd(al)] + " и " + a[rnd(al)] + " " +
        b[rnd(bl)] + ", а также " + a[rnd(al)] + " " + b[rnd(bl)] +
        " " + c[rnd(cl)];
    }
    return o;
}

function caps(target) {
    if (!target) {
        return target;
    }
    return target[0].toUpperCase() + target.slice(1);
}

function fill_text() {
    var amount = document.getElementById("number").value;
    if (amount >= 500) {
        alert('Генерирование большого количества записей может занять' +
        +'длительное время. Сократите число и повторите попытку.');
    } else if (amount == 0) {
        alert('Количество генерируемых записей не может быть равным' +
        '0 или отрицательным. Повторите попытку.');
    } else {
        var text = "";
        document.getElementsByTagName("thead")[0].innerHTML = '<tr>' +
        '<th class="num collapse in">№</th><th>Текст</th></tr>';
        for (var i = 0; i < amount; i++) {
            text += '<tr><td class="num collapse in" id="number">' + ((i<9) ? "0"+(i+1) : (i+1)) + '.</td><td onclick="document.execCommand(\'selectAll\',false,null);"contenteditable>' + get_string(rnd(10)) + '.</td></tr>';
        }
        document.getElementsByTagName("tbody")[0].innerHTML = text;
    }
    list_is_empty = 0;
}

window.onload = function() {
    request_database();
    document.getElementById('reset').onclick = function() {
        document.getElementsByTagName("thead")[0].innerHTML = "";
        document.getElementsByTagName("tbody")[0].innerHTML = "";
        list_is_empty = 1;
    }
    document.getElementById('toogle').onclick = function() {
        if (list_is_empty != 0) {
            if (num_toogle == 1) {
                document.querySelectorAll('th')[0].style.display = 'none';
                for (var i = 0; i < document.getElementById("number").value; i++) {
                    document.querySelectorAll('table tr')[i + 1].querySelectorAll('td')[0].style.display = 'none';
                }
                document.getElementById('toogle').setAttribute("class","btn btn-default disabled");
                num_toogle = 0;
            } else {
                document.querySelectorAll('th')[0].style.display = 'table-cell';
                for (var i = 0; i < document.getElementById("number").value; i++) {
                    document.querySelectorAll('table tr')[i + 1].querySelectorAll('td')[0].style.display = 'table-cell';
                }
                document.getElementById('toogle').setAttribute("class","btn btn-default enabled");
                num_toogle = 1;
            }
        }
    }
    document.getElementsByClassName('btn-ok')[0].onclick = function() {
        fill_text();
    }
    document.getElementsByClassName("btn-scroll")[0].onclick = function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    }
};

window.onscroll = function() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementsByClassName("btn-scroll")[0].style.display = "block";
    } else {
        document.getElementsByClassName("btn-scroll")[0].style.display = "none";
    }
};
